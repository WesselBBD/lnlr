extends Node2D

onready var floorG = $Floor
onready var bgMusic = $BackgroundMusic
onready var spawner = $EnemySpawner
onready var globalCamera = $GlobalCamera

signal startGame
signal endGame

var Exit = preload ("res://assets/ui/EndScreen.tscn")

func _ready():
	# DO NOT ADD ADDTIONAL DANCE TILES TO THE WORLD
	initLane(Global.blueTile)
	initLane(Global.greenTile)
	initLane(Global.redTile)
	Global.CreateSections()
	Global.TilesLoaded = true
	$GlobalCamera.current = true
	Global.zcounter = 0
	pass

func initLane(TileName: String):
	# not included: logic to determine if some other tile is a part of the field
	var localLine = floorG.get_used_cells_by_id(floorG.tile_set.find_tile_by_name(TileName))
	localLine.sort() #might not be required
	var worldLine = Array()
	for tile in localLine:
		worldLine.append(floorG.map_to_world(tile)) # might need to be global
	var laneId := Global.LineToLaneID(TileName)
	Global.Lines[laneId] = worldLine
	print(TileName," added!")

func _entered_the_fog(enemy):
	if enemy.is_in_group("Enemy"):
		enemy.InAoE(Global.AOE.SLOW, Global.FogPeriod, Global.FogSpeedReduction, Global.FogDamage)

func _exited_the_fog(enemy):
	if enemy.is_in_group("Enemy"):
		enemy.OutAoE(Global.AOE.SLOW, Global.FogSlowLingerTime)

func _entered_the_sparks(enemy):
	if enemy.is_in_group("Enemy"):
		enemy.InAoE(Global.AOE.ENERGIZED, Global.EnergizedEffectLingerTime, Global.EnergizedSpeedIncrease)

func _exited_the_sparks(enemy):
	if enemy.is_in_group("Enemy"):
		enemy.OutAoE(Global.AOE.ENERGIZED, Global.EnergizedEffectLingerTime)

var AoESpot = preload ("res://assets/components/aoeSpot/AOESpot.tscn")

func _on_enemyDeath(name, diePosition):
	if diePosition == null:
		print("You died in the void...")
		return
	match name:
		Global.Enemies.ST0RM:
			Global.score += 50
			var sparkSpot = AoESpot.instance()
			sparkSpot.get_node("LifeTime").wait_time = Global.SparksLingerTime #TODO: Better
			sparkSpot.AoEType = Global.AOE.ENERGIZED
			sparkSpot.position = diePosition
			sparkSpot.connect("body_entered",self,"_entered_the_sparks")
			sparkSpot.connect("body_exited",self,"_exited_the_sparks")
			add_child(sparkSpot)
		Global.Enemies.BRUISR:
			Global.score += 1000
			$CroudCheer.play()
		Global.Enemies.ENFORCER:
			Global.score += 10
	pass

var bgMusicFiles = [
	preload ("res://media/Music/Game/alex-productions-cyberpunk-computer-game-idra.mp3"),
	preload ("res://media/Music/Game/alex-productions-extreme-sport-dubstep-im-back.mp3"),
	preload ("res://media/Music/Game/Enigma-Long-Version-Complete-Version.mp3"),
	preload ("res://media/Music/Game/Extreme-Sport-Trap-Music-PISTA.mp3"),
	preload ("res://media/Music/Game/Gaming.mp3"),
	preload ("res://media/Music/Game/Hidden-Official-.mp3"),
	preload ("res://media/Music/Game/Powerful-Trap-.mp3")
	]

#used for main bg music loop
func _on_BackgroundMusic_finished():
	RandPlayBG(0)

func _on_EndZone_body_entered(body):
	if body.is_in_group("Enemy"):
		emit_signal("endGame")
		get_tree().change_scene_to(Exit)
	pass

func RandPlayBG(time: float):
	var randMusic = randi() % bgMusicFiles.size()
	$BackgroundMusic.stream = bgMusicFiles[randMusic]
	$BackgroundMusic.play(time)

func _on_GameStart_timeout():
	print("start game")
	emit_signal("startGame")
	
	RandPlayBG(54)
	
	$MusicRamp.interpolate_property($BackgroundMusic, "volume_db", -2, -12, 10)
	$MusicRamp.start()
	yield($MusicRamp, "tween_completed")
	$MusicRamp.stop_all()
	pass
