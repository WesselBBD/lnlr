extends Camera2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var level = get_parent()
var stopPos
var gamePlayZoom: Vector2
var gamePlayPosition: Vector2

var positionDelta
var zoomDelta
var frames = 20
var frameCounter = 20
# Called when the node enters the scene tree for the first time.
func _ready():
	stopPos = level.get_node("StopPos")
	gamePlayPosition = stopPos.position
	gamePlayZoom = stopPos.zoom
	level.connect("startGame",self,"_startGame")


func _startGame():
	#setPositionDelta()
	#setZoomDelta()
	#frameCounter = 0
	pass
	
	
func setZoomDelta():
	zoomDelta = ( gamePlayZoom - self.zoom) / frames
	
func setPositionDelta():
	positionDelta = ( gamePlayPosition - self.position) / frames
	
	
func _process(delta):
	if frameCounter < frames:
		changePosition()
		changeZoom()
		frameCounter = frameCounter + 1

func changePosition():
	var currentPosition = self.position
	var newPosition = currentPosition + positionDelta
	self.set_position(newPosition)

func changeZoom():
	var currentZoom = self.zoom
	var newZoom = currentZoom + zoomDelta
	self.set_zoom(newZoom)


	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_GameStart_timeout():
	setPositionDelta()
	setZoomDelta()
	frameCounter = 0
	pass # Replace with function body.
