extends "res://assets/components/CoreCreature.gd"

var AttackLine: Array

export(Global.Enemies) var EnemyType
var MyLaneID = Global.SafeLaneId
var EnforcingThePeace = false #switch out for the 2d screen visable thing

signal EnemyDied(type, position)

func _creature_preready():
	MyLaneID = Global.GetRandomLane()
	AttackLine = Global.MakeLaneCopy(MyLaneID)
	
	if AttackLine.size() != 0:
		TargetPos = AttackLine.front()
	else:
		TargetPos = Vector2.ZERO
	
	self.collision_layer = 0
	self.set_collision_layer_bit(MyLaneID, true)
	self.set_collision_mask_bit(MyLaneID+Global.AttackOffset, true) #0 indexed
	var upper = 100*(3 - MyLaneID)
	var oneoff = upper - 1
	var zc = Global.zcounter
	Global.zcounter += 1
	self.z_index = (oneoff - (zc % 99))


func _physics_process(_delta):
	if TargetPos == null:
		dieDramatically("there is nowhere to go to anymore")
		return
	
	if self.position.distance_to(TargetPos) > (3 + speed):
		var velocity = global_position.direction_to(TargetPos) * speed
		velocity.round()
		var colInfo = move_and_collide(velocity)

		if colInfo != null:
			dieDramatically("i colided with something")
	else:
		TargetPos = AttackLine.pop_front()


func _die(reason):
	emit_signal("EnemyDied", EnemyType, TargetPos)
	._die(reason)



func _on_attack(type,lane):
	if lane == MyLaneID:
		match type:
			Global.PlayerAttacks.LAZER:
				takeDramaticDamage(Global.LazerDamage)


#bool -> continue?
func _takeDamage(damage) -> bool:
	if !EnforcingThePeace:
		return false
	#print("i took ", damage, " damage!")
	return ._takeDamage(damage)


func _on_screen_entered():
	EnforcingThePeace = true
	pass


func _on_frame_changed():
	pass # Replace with function body.
