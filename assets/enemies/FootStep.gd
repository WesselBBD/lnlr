extends AudioStreamPlayer2D

#export(Array, int) var GroundFrames = []
export(int) var PlayFrame = 0 
onready var anim = get_parent().get_node("Offset").get_node("AnimatedSprite")

var basePitch: float
export(float) var pitchFloor: float = 0.7
export(float) var pitchRoof: float = 1.3
func _ready():
	basePitch = self.pitch_scale

func _on_AnimatedSprite_frame_changed():
	var CurAnim = anim.animation
	if CurAnim == "Walking":
		var frame = anim.frame
		if frame == PlayFrame:
			var between30 = (randi() % 60) - 30
			var newPitch = basePitch + (between30 / 100.0)
			pitch_scale = clamp(newPitch, pitchFloor, pitchRoof)
			play()
