extends KinematicBody2D

onready var anim: AnimatedSprite = $Offset/AnimatedSprite
onready var TargetPos = Global.SpawnSafePoint
onready var sCtl = $SoundController
onready var Colidy: CollisionShape2D = $CollisionShape2D
onready var Damge: Tween = $DamageTween

export var defaultSpeed = 5.0
export var health = 5
export var blowBack = 50

var speed = 0.0
var JustDamaged = false
var EffectsApplied: Array = Array()

signal TookDamage(damage)
signal DotDamage(damage, effectid, period)

func _creature_preready():
	print(name, " is almost ready!")

func _ready():
	#print("i think therefore...")
	
	set_physics_process(false)
	speed = defaultSpeed
	EffectsApplied.clear()
	EffectsApplied.resize(Global.AOE.size())
	EffectsApplied.fill(false)

	anim.playing = true
	anim.animation = "Idle"
	
	_creature_preready()
	
	connect("TookDamage",self,"_took_damage")
	connect("DotDamage",self,"_on_dotdamage")

	anim.animation = "Walking"
	
	sCtl.DefaultMightPlaySound(sCtl.Sounds.ENTRY)
	
	set_physics_process(true)


func dieDramatically(reason):
	set_physics_process(false) #turn off core movement logic
	if is_instance_valid(Colidy):
		Colidy.queue_free()
	anim.animation = "Die"
	if sCtl.DefaultMightPlaySound(sCtl.Sounds.DIE):
		yield(sCtl,"finished") #wait for the dying words fore actually dying
	else: #animation always hapens faster
		yield(anim, "animation_finished") #wait for the dramatic final curtain call
	_die(reason)

func _die(reason):
	#print("i die becuase ", reason)
	queue_free()

#AOE

func OutAoE(effectType,lingerTime):
	if lingerTime > 0:
		$LingerTimer.start(lingerTime)
		yield($LingerTimer, "timeout")
	EffectsApplied[effectType] = false
	speed = defaultSpeed
	#match effectType:
	#	Global.AOE.SLOW:
	#		speed = defaultSpeed
	#	Global.AOE.ENERGIZED:
	#		speed = defaultSpeed

func InAoE(effectType, effectPeriod, effectValue, effectDamage: int = 0):
	EffectsApplied[effectType] = true
	if effectDamage > 0:
		emit_signal("DotDamage", effectDamage, effectType, effectPeriod)
	match effectType:
		Global.AOE.SLOW:
			speed *= effectValue
		Global.AOE.ENERGIZED:
			speed *= effectValue
	print(speed)

func _on_dotdamage(damage, effectId, period):
	if EffectsApplied[effectId]:
		_takeDamage(damage)
		yield(get_tree().create_timer(period), "timeout")
		emit_signal("DotDamage", damage, effectId, period) #potential more/less damage over time
	pass

#----

#bool -> continue?
func _takeDamage(damage) -> bool:
	if JustDamaged:
		return false
	health -= damage #actually take damage
	emit_signal("TookDamage",damage)
	if shouldDie():
		return false # we took damage but we are dying, don't continue
	return true

func takeDramaticDamage(damage):
	if !_takeDamage(damage):
		return #will return if we are not active or are dying or we were just damaged
	set_physics_process(false)
	anim.animation = "Hit"
	yield(anim, "animation_finished")
	anim.animation = "Walking"
	set_physics_process(true) #NOTE: if we should die this is not set back

func _took_damage(_damage):
	if !JustDamaged:
		JustDamaged = true
		Damge.interpolate_property(anim, "self_modulate", Color.white, Color(1,0.47,0.47,1), 0.5)
		Damge.start()
		$ISeconds.start()
		yield($ISeconds, "timeout")
		Damge.stop_all()
		anim.self_modulate = Color.white
		if is_instance_valid(self): #we might not exist anymore
			JustDamaged = false

func takeRecipracleDamage(damage, blowBackCallback: FuncRef):
	blowBackCallback.call_func(blowBack)
	takeDramaticDamage(damage)

func shouldDie() -> bool:
	if health <= 0:
		dieDramatically("my health is too low")
		return true
	return false
