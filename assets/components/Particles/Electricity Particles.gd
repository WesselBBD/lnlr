extends Node2D

export(Vector2) var Extents = Vector2(500,500)

func _ready():
	$ElectricParticle.emission_rect_extents = Extents
	$ElectricPulse.emission_rect_extents = Extents
