extends AudioStreamPlayer2D


export(Array, AudioStream) var DieTracks
export(Array, AudioStream) var EntryTracks
export(Array, AudioStream) var InteractTracks
export(Array, AudioStream) var HurtTracks


enum Sounds {DIE, ENTRY, INTERACT, HURT}
var Tracks = []

export(float, 0, 100.0) var DieDropPercentage = 45.0
export(float, 0, 100.0) var EntryDropPercentage = 50.0
export(float, 0, 100.0) var InteractDropPercentage = 100.0
export(float, 0, 100.0) var HurtDropPercentage = 45.0

var DefaultPercentages = []

func _ready():
	Tracks = [DieTracks,EntryTracks,InteractTracks,HurtTracks]
	DefaultPercentages = [DieDropPercentage,EntryDropPercentage,InteractDropPercentage,HurtDropPercentage]

func _RandomSound(track: Array):
	stream = track[randi() % track.size()]
	
	play()

func DefaultMightPlaySound(soundType: int) -> bool:
	return MightPlaySound(soundType, DefaultPercentages[soundType])

func MightPlaySound(soundType: int, keepPercentage: float = 0.0) -> bool:
	if (randf() < (keepPercentage / 100.0)):
		return false #did not play
	else:
		MustPlaySound(soundType)
	return true #played

func MustPlaySound(soundType: int):
	_RandomSound(Tracks[soundType])


