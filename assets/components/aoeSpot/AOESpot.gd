extends Area2D


onready var LTT: Timer = $LifeTime
export(Global.AOE) var AoEType

onready var Eparts = [$ElectricityParticle, $ElectricityParticle2, $ElectricityParticle3]
onready var Sparts = [$FogParticles, $FogParticles2, $FogParticles3]

func _ready():
	for child in Eparts:
		child.visible = (AoEType == Global.AOE.ENERGIZED)
	for child in Sparts:
		child.visible = (AoEType == Global.AOE.SLOW)

func _on_LifeTime_timeout():
	queue_free()

func _process(delta):
	pass

func StopTimer():
	LTT.stop()

func SetTimer(time):
	LTT.wait_time = time

func StartTimer():
	LTT.start()
