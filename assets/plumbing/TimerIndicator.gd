extends Sprite

enum INDICATORSTATE {OFF, ON, SUCCESS, FAILURE}

func _ready():
	pass

func turnOn():
	setState(INDICATORSTATE.ON)

func blinkSuccess():
	blinkState(INDICATORSTATE.SUCCESS)

func blinkState(IndicatorState: int = 0):
	setState(IndicatorState)
	yield(get_tree().create_timer(0.25), "timeout")
	if is_instance_valid(self): #when you come back you might not exist anymore...
		setState(INDICATORSTATE.ON)

func setState(IndicatorState: int = 0):
	self.frame = IndicatorState
