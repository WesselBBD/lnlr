extends Node

onready var Woofer = preload ("res://assets/attack/Woofer/Woofer.tscn")
onready var Level = get_parent()

func _on_Level_PlayerAttack(type, segment):
	if type == Global.PlayerAttacks.WOOFER:
		spawnWoofer()
	pass # Replace with function body.

func spawnWoofer():
	Level.add_child(Woofer.instance())
