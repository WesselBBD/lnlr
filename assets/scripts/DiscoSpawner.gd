extends Node

onready var Level = get_parent()
onready var Disco = preload ("res://assets/attack/Disco/Disco.tscn")

	var newDisco = Disco.instance()
	newDisco.MyLaneID = lane
	Level.add_child(newDisco)

func _on_Level_PlayerAttack(type, segment):
	if type == Global.PlayerAttacks.DISCO:
		SpawnLane(Global.SegementToLane(segment))
