extends Sprite

export(Global.Lanes) var Lane

onready var Lazer = $LazerPinkish

func FireLazer(timeout):
	Lazer.set_collision_mask_bit(Lane,true)
	
	Lazer.fireLazer(true)
	yield(get_tree().create_timer(Global.LazerTimeout), "timeout")
	Lazer.fireLazer(false)
	Lazer.set_collision_mask_bit(Lane,false)
