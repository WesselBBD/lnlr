extends Node2D

onready var children = get_children()

func _on_Control_PlayerAttack(type, segment):
	if type == Global.PlayerAttacks.LAZER:
		for child in children:
			var lane = Global.SegementToLane(segment)
			if child.Lane == lane:
				child.FireLazer(Global.LazerTimeout)
	pass
