extends Path2D

export var spawning = false
var BRUISR = preload ("res://assets/enemies/BRUIS.R/BRUISR.tscn")
var ENFORCER = preload ("res://assets/enemies/ENFORC.ER/ENFORCER.tscn")
var ST0RM = preload ("res://assets/enemies/ST0.RM/ST0RM.tscn")

var Bots = [ENFORCER, ST0RM, BRUISR]

onready var spwn = $SpawnerPath
onready var enfspwn = $EnforcerSpawnerTimer
onready var strmspawn = $StormSpawnerTimer
onready var dtick = $DifficultyTick
onready var sAnnounce = $StormAnnouncement
onready var bAnnounce = $BruisrAnnouncement
onready var level = get_parent()
onready var control = level.get_node("ControlLayer/Control")

func _ready():
	while !Global.TilesLoaded:
		print("tiles not yet loaded, waiting")
		yield(get_tree().create_timer(1), "timeout")
	print("tiles loaded!")
	level.connect("startGame",self,"_startGame")
	level.connect("endGame",self,"_endGame")
	firstWave()
	randomize()

func firstWave():
	_spawn(Global.Enemies.ENFORCER)
	_spawn(Global.Enemies.ENFORCER)
	_spawn(Global.Enemies.ENFORCER)
	_spawn(Global.Enemies.ENFORCER)
	_spawn(Global.Enemies.ENFORCER)
	_spawn(Global.Enemies.ENFORCER)

#signal sets spawning enabled
func _endGame():
	print("spawning turned off")
	spawning = false
	
func _startGame():
	print("spawning turned on")
	enfspwn.start()
	strmspawn.start()
	dtick.start()
	spawning = true #enable from signal instead

func _spawn(bot):
	#if !spawning:
	#	return
	
	var newBot = Bots[bot].instance()
	var random = randi()
	spwn.offset = random
	newBot.position = spwn.position
	control.connect("PlayerAttack",newBot,"_on_attack")
	newBot.connect("EnemyDied",level,"_on_enemyDeath")
	
	newBot.health *= Global.GlobalHealthMultiplier
	newBot.defaultSpeed *= Global.GlobalSpeedMultiplier
	
	level.add_child(newBot)


func _on_EnforcerSpawnerTimer_timeout():
	if !spawning:
		return
	_spawn(Global.Enemies.ENFORCER)
	pass # Replace with function body.


func _on_StormSpawnerTimer_timeout():
	if !spawning:
		return
	sAnnounce.play()
	_spawn(Global.Enemies.ST0RM)
	pass # Replace with function body.


func _on_DifficultyTick_timeout():
	if !spawning:
		return
	#TODO: put in limits
	Global.GlobalHealthMultiplier += 0.1
	Global.GlobalSpeedMultiplier += 0.1
	_spawn(Global.Enemies.BRUISR)
	bAnnounce.play()
	Global.score *= 1.4 #survive bonus
	print("Difficulty Spike> GHealth:",Global.GlobalHealthMultiplier, " GSpeed:", Global.GlobalSpeedMultiplier, " Score:", Global.score)
	pass # Replace with function body.
