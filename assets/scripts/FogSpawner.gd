extends Node

onready var Level = get_parent()
onready var Fogger = preload ("res://assets/attack/Foger/Foger.tscn")

func SpawnFogger(area):
	var newFogger = Fogger.instance()
	newFogger.position = Global.GetRandomPositionInBlock(area)
	Level.add_child(newFogger)
	yield(newFogger.AoE.LTT, "timeout")


func _on_Level_PlayerAttack(type, segment):
	if type == Global.PlayerAttacks.FOG:
		SpawnFogger(Global.SegmentToArea(segment))
