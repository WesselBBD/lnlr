extends Node

onready var Bouncer = preload ("res://assets/attack/bouncer/Bouncer.tscn")
onready var Woofer = preload ("res://assets/attack/Woofer/Woofer.tscn")
onready var Disco = preload ("res://assets/attack/Disco/Disco.tscn")
onready var Fogger = preload ("res://assets/attack/Foger/Foger.tscn")
onready var Level = get_parent()

func _on_Level_PlayerAttack(type, segment):
	match type:
		Global.PlayerAttacks.WOOFER:
			SpawnWoofer()
		Global.PlayerAttacks.BOUNCER:
			SpawnBouncer(Global.SegementToLane(segment))
		Global.PlayerAttacks.DISCO:
			SpawnDisco(Global.SegementToLane(segment))
		Global.PlayerAttacks.FOG:
			SpawnFogger(Global.SegmentToArea(segment))


func SpawnWoofer():
	Level.add_child(Woofer.instance())

func SpawnBouncer(lane):
	var newBouncer = Bouncer.instance()
	var blockCellPos = Global.GetRandomPositionInLaneBlock(lane, Global.Areas.CLOSE)
	newBouncer.TargetPos = blockCellPos
	newBouncer.MyLaneID = lane
	Level.add_child(newBouncer)

func SpawnDisco(lane):
	var newDisco = Disco.instance()
	newDisco.MyLaneID = lane
	Level.add_child(newDisco)

func SpawnFogger(area):
	var newFogger = Fogger.instance()
	newFogger.position = Global.GetRandomPositionInBlock(area)
	newFogger.get_node("AOESpot").connect("body_entered",Level,"_entered_the_fog")
	newFogger.get_node("AOESpot").connect("body_exited",Level,"_exited_the_fog")
	Level.add_child(newFogger)
