extends AnimatedSprite

onready var AoE = $AOESpot

func _ready():
	AoE.StopTimer()
	AoE.SetTimer(Global.FoggerTimeout)
	AoE.LTT.connect("timeout", self, "_on_lifetimeout")
	AoE.StartTimer()

func _on_lifetimeout():
	self.queue_free()
