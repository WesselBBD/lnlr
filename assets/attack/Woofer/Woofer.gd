extends Node2D

var CenterPath: Array
export var duration = 0.2
var TargetPos

func _ready():
	CenterPath = Global.MakeLaneCopy(Global.Lanes.CENTER)
	position = CenterPath.back()
	TargetPos = CenterPath.pop_back()
	Start()

#TODO: the hec did I do here

func Start():
	if TargetPos == null:
		queue_free()
	$Tween.interpolate_property(self, "position", position, TargetPos ,duration)
	$Tween.start()
	yield($Tween,"tween_all_completed")
	TargetPos = CenterPath.pop_back()
	Start()


func _on_crossSquare_body_entered(body):
	if body.is_in_group("Enemy"):
		body.takeDramaticDamage(Global.WooferDamage)
