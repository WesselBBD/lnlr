extends "res://assets/attack/floatDown.gd"
#NOTE: if the bouncer reaches his position he turns off his physics
#NOTE: Bouncer has a small grace period here they can hurt enemies without taking blowback
#	   If not desired overide _took_damage

var SpawnPos: Vector2
var Dying: bool = false

onready var ctlSound = $SoundController

export var health = 20

func _ready():
	SpawnPos = position
	set_collision_layer_bit(MyLaneID+Global.AttackOffset, true) #0 indexed
	ctlSound.MustPlaySound(ctlSound.Sounds.ENTRY)
	._ready()

func _physics_process(delta):
	if Dying:
		velocity.y += gravity
		position += (velocity * delta)
		return
	
	if position.distance_to(TargetPos) < (3):
		set_physics_process(false)
	
	velocity = global_position.direction_to(TargetPos) * speed
	velocity.round()
	position += (velocity * delta)

func TakeBlowBack(damage):
	ctlSound.MightPlaySound(ctlSound.Sounds.HURT)
	health -= damage
	if health <= 0:
		dieDramatically()

func dieDramatically():
	$CollisionShape2D.queue_free()
	ctlSound.DefaultMightPlaySound(ctlSound.Sounds.DIE)
	$Pop.play()
	set_physics_process(true)
	Dying = true
	velocity.y = -100
	velocity.x = 200
	ctlSound.DefaultMightPlaySound(ctlSound.Sounds.DIE)


func _on_VisibilityNotifier2D_screen_exited():
	if !Decending:
		queue_free()

onready var ass = funcref(self, "TakeBlowBack")

func _on_Bouncer_body_entered(body):
	if body.is_in_group("Enemy"):
		body.takeRecipracleDamage(Global.BouncerDamage, ass)
	pass
