extends Area2D

var MyLaneID = Global.SafeLaneId
var TargetPos = Global.SpawnSafePoint

export(float) var HeavensHight = 1000
export(float) var DecendSpeed = 1000
var speed = 0.0

onready var anim = $AnimatedSprite

var Decending = true
var fromTheHeavens: Vector2
var velocity: Vector2

func _ready():
	fromTheHeavens = TargetPos
	fromTheHeavens.y -= HeavensHight
	position = fromTheHeavens
	
	Decending = true
	speed = DecendSpeed
	self.z_index = (3-MyLaneID)*100

func dieDramatically():
	queue_free()
