# <3 GDQuest
# DO NOT ACTIVATE SHORTER THEN 0.5s IDK WHY BUT THE TWEEN REACTS WEIRDLY
extends RayCast2D

var is_firing = false
export var lazerGirth = 10.0
export var lazerGrowthSpeed = 0.2
export var lazerShrinkSpeed = 0.1
export(Global.Lanes) var laneId = 0 #should be posible to self determine but codecode

onready var L: Line2D = $Line2D
onready var Tw: Tween = $Tween
onready var of: CPUParticles2D = $OriginFlare
onready var hp: CPUParticles2D = $HitParticles
onready var bp: CPUParticles2D = $BeamParticles

func _ready():
	set_physics_process(false)
	L.points[1] = Vector2.ZERO

func _physics_process(_delta):
	var cast_point := cast_to
	force_raycast_update()
	
	var isColiding = is_colliding()
	
	if isColiding:
		hp.emitting = true
		cast_point = to_local(get_collision_point())
		hp.global_rotation = get_collision_normal().angle()
		hp.position = cast_point
	
	var beampStop = cast_to #cast_point <- use instead if you want to stop at the enemy
	L.points[1] = beampStop #stop at hit
	bp.position = beampStop * 0.5
	bp.emission_rect_extents.x = beampStop.length() * 0.5

func fireLazer(fire: bool):
	is_firing = fire
	
	of.emitting = is_firing
	bp.emitting = is_firing
	
	set_physics_process(is_firing)
	if (is_firing):
		appear()
	else:
		hp.emitting = false
		disappear()


func appear():
	Tw.interpolate_property(L, "width", 0.0, lazerGirth, lazerGrowthSpeed)
	Tw.start()

func disappear():
	Tw.interpolate_property(L, "width", lazerGirth, 0.0, lazerShrinkSpeed)
	Tw.start()


#func _unhandled_input(event):
#	if event is InputEventMouseButton:
#		self.fireLazer(event.pressed)



