extends "res://assets/attack/floatDown.gd"

var AttackLine = []

export(float) var RollSpeed = 5.5

onready var l = $Line2D

enum LinePoints {TETHER, ANCOR}

#onready var LineStart = $Line2D.points[1]

func _ready():
	AttackLine = Global.MakeLaneCopy(MyLaneID)
	
	if AttackLine.size() != 0:
		TargetPos = AttackLine.back()
		fromTheHeavens = TargetPos
		fromTheHeavens.y -= HeavensHight
		position = fromTheHeavens
	else:
		TargetPos = Vector2.ZERO
	
	Decending = true
	speed = DecendSpeed
	
	set_collision_layer_bit(MyLaneID+Global.AttackOffset, true) #0 indexed

func _process(_delta):
	if !Decending:
		#anim.play_backwards()
		l.queue_free()
		speed = RollSpeed
		set_process(false)

var Dying = false

func _physics_process(delta):
	if Dying:
		velocity.y += gravity
		position += (velocity * delta)
		return
	
	if TargetPos == null:
		queue_free()
		#dieDramatically()
		return
		
	if position.distance_to(TargetPos) > (30):
		velocity = global_position.direction_to(TargetPos) * speed
		velocity.round()
		position += (velocity * delta)
	else:
		Decending = false
		TargetPos = AttackLine.pop_back()

func dieDramatically():
	Dying = true
	$CollisionShape2D.queue_free()
	#queue_free()


func _on_DiscoBall_body_entered(body):
	if body.is_in_group("Enemy"):
		body.takeDramaticDamage(Global.DiscoDamage)
		dieDramatically()
	pass
