extends Node2D

onready var at = $AnimationTree
var statemachine

func _ready():
	at.active = true
	statemachine = at["parameters/playback"]
	statemachine.start("Idle")
