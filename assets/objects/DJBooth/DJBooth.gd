extends Node2D

onready var dj = $DJ

func _on_Level_PlayerAttack(_type, lane):
	match lane:
		Global.Lanes.RIGHT:
			dj.statemachine.travel("ScratchRight")
		Global.Lanes.LEFT:
			dj.statemachine.travel("ScratchLeft")
		Global.Lanes.CENTER:
			if (randi() % 2) == 0:
				dj.statemachine.travel("ScratchLeft")
			else:
				dj.statemachine.travel("ScratchRight")
