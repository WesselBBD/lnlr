extends Node2D

onready var Lights = get_children()
export var intensity = 0.01
export var target = 0.1
export var maxSize = 0.3

func _ready():
	print("ready")

func _process(_delta):
	for light in Lights:
		light.texture_scale = target #lerp(light.texture_scale, target, intensity)

func _on_newBlink_timeout():
	target = fmod(randf(), maxSize)
