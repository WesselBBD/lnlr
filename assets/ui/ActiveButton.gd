extends TextureButton

func _on_Control_PlayerAttack(type, segment):
	if pressed:
		pressed = false
		disabled = true
		yield(get_tree().create_timer(Global.AbilityCooldown), "timeout")
		disabled = false
	pass
