extends Sprite

var GameLevel = load ("res://assets/Level.tscn")

func _ready():
	$Label.text = str(ceil(Global.score))

func _input(event):
	if event is InputEventKey and event.is_pressed():
		Global.score = 0
		get_tree().change_scene_to(GameLevel)
