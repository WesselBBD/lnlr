extends Node2D

onready var mm = $MainMenuEmpty
onready var tit = $Title
onready var inst = $Instructions
onready var us = $SpaceBar
onready var mblink = $MenuBlink
onready var tim = $Timer
onready var sound = $AudioStreamPlayer2D
onready var intro = $Intro

onready var it = $IntroTime


var ps = preload ("res://media/UI/Pressed - Spacebar.png")


var GameLevel = preload  ("res://assets/Level.tscn")

var SeenInstructions = false
var Interacted = false

func _ready():
	$Camera2D.current = true
	pass

var lock = false

func _input(event):
	if event is InputEventKey and event.is_pressed():
		if !Interacted:
			$Click.play()
			StartFade()
			Interacted = true
		if SeenInstructions && !lock:
			lock = true
			mblink.interpolate_property(inst, "self_modulate", inst.self_modulate, Color(1,1,1,0), 1.5)
			mblink.interpolate_property(mm, "self_modulate", mm.self_modulate, Color(1,1,1,0), 2)
			us.visible = false
			mblink.start()
			yield(mblink,"tween_all_completed")
			mblink.stop_all()
			mblink.interpolate_property(intro, "self_modulate", intro.self_modulate, Color.white, 5)
			mblink.start()
			yield(mblink,"tween_all_completed")
			mblink.stop_all()
			it.start()
			yield(it, "timeout")
			get_tree().change_scene_to(GameLevel)
	pass


func _on_ReadInstructions_timeout():
	#using the fact that this loops
	if !lock:
		SeenInstructions = true
		mblink.interpolate_property(us, "self_modulate", Color(1,1,1,0), Color(1,1,1,1), 3, Tween.TRANS_LINEAR,Tween.EASE_OUT)
		mblink.start()
	else:
		us.self_modulate = Color(1,1,1,0)
	pass # Replace with function body.


var smothing = [Color.white, Color(1,1,1,0)]

func _on_Timer_timeout():
	StartBlink()
	pass # Replace with function body.

func StartFade():
	mblink.interpolate_property(tit, "self_modulate", tit.self_modulate, Color(1,1,1,0), 3)
	mblink.start()
	yield(mblink,"tween_all_completed")
	tit.visible = false
	mblink.stop_all()
	#mblink.interpolate_property(mm, "self_modulate", null, Color(1,1,1,0), 3, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	mblink.interpolate_property(inst, "self_modulate", Color(1,1,1,0.0), Color.white, 3, Tween.TRANS_LINEAR,Tween.EASE_IN)
	mblink.start()
	yield(mblink,"tween_all_completed")
	print("tween done")
	$ReadInstructions.start()

func StartBlink():
	mblink.interpolate_property(tit, "self_modulate", Color.white, Color(1,1,1,0.5), 1.6, Tween.TRANS_BOUNCE,Tween.EASE_OUT_IN)
	mblink.start()
	yield(mblink,"tween_all_completed")
	smothing = [Color(1,1,1,0), Color.white]
	StartBlink()
	#stackoverflow after a few years

