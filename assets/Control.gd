extends Control

signal PlayerAttack(type, segment)
signal newAttacks(playerAttacks)


var CurrentAttacksAvailable = [Global.PlayerAttacks.WOOFER, Global.PlayerAttacks.FOG, Global.PlayerAttacks.WOOFER]
var ChosenAttack = -1
var ChosenSegment = -1
var OnBigCoolDown = false

func _ready():
	set_process_input(false)
	pass

func _input(event):
	if ChosenAttack > -1 && ChosenSegment > -1:
		MakeAttack(ChosenAttack,ChosenSegment)
		ChosenAttack = -1
		ChosenSegment = -1
		return
	
	if event.is_action_pressed("player_section_a"):
		ChosenSegment = Global.Sections.A
		$LeftButton.pressed = true
		$UpButton.pressed = false
		$RightButton.pressed = false

	if event.is_action_pressed("player_section_b"):
		ChosenSegment = Global.Sections.B
		$LeftButton.pressed = false
		$UpButton.pressed = true
		$RightButton.pressed = false

	if event.is_action_pressed("player_section_c"):
		ChosenSegment = Global.Sections.C
		$LeftButton.pressed = false
		$UpButton.pressed = false
		$RightButton.pressed = true


	if event.is_action_pressed("player_play_a"):
		if !$AButton.disabled:
			ChosenAttack = CurrentAttacksAvailable[0]
			$AButton.pressed = true
			$Frame1.pressed = true
			$WButton.pressed = false
			$Frame2.pressed = false
			$DButton.pressed = false
			$Frame3.pressed = false

	if event.is_action_pressed("player_play_b"):
		if !$WButton.disabled:
			ChosenAttack = CurrentAttacksAvailable[1]
			$AButton.pressed = false
			$Frame1.pressed = false
			$WButton.pressed = true
			$Frame2.pressed = true
			$DButton.pressed = false
			$Frame3.pressed = false

	if event.is_action_pressed("player_play_c"):
		if !$DButton.disabled:
			ChosenAttack = CurrentAttacksAvailable[2]
			$AButton.pressed = false
			$Frame1.pressed = false
			$WButton.pressed = false
			$Frame2.pressed = false
			$DButton.pressed = true
			$Frame3.pressed = true
		
	if event.is_action_pressed("player_play_d"):
		$SpaceBar.pressed = true
		yield(get_tree().create_timer(15), "timeout")
		$SpaceBar.pressed = false

func MakeAttack(attack, segment):
	print("Attack=", ChosenAttack, " Segment=", ChosenSegment)
	emit_signal("PlayerAttack", attack, segment)
	RandomizeAttacks()

#enum PlayerAttacks {LAZER, FOG, BOUNCER, DISCO, WOOFER}
onready var FrameSet = [
		[preload ("res://media/UI/Unpressed - Laser.png"),
		 preload ("res://media/UI/Pressed - Laser.png") ],
		[preload ("res://media/UI/Unpressed - Fog.png"),
		 preload ("res://media/UI/Pressed - Fog.png") ],
		[preload ("res://media/UI/Unpressed - Bouncer.png"),
		 preload ("res://media/UI/Pressed - Bouncer.png") ],
		[preload ("res://media/UI/Unpressed - Disco.png"),
		 preload ("res://media/UI/Pressed - Disco.png") ],
		[preload ("res://media/UI/Unpressed - Woofer.png"), 
		 preload ("res://media/UI/Pressed - Woofer.png") ],
	]

onready var Buttons = [$AButton, $WButton, $DButton]

func RandomizeAttacks():
	var newAttacks = []
	var totalPA = Global.PlayerAttacks.size()
	var vals = Global.PlayerAttacks.values()
	
	for i in range(3):
		if Buttons[i].disabled:
			CurrentAttacksAvailable[i] = vals[randi() % totalPA]

	#animate random attack

	SetFrames()
	#ding

var EmptyNormalFrame = preload ("res://media/UI/Unpressed - Card.png")
var EmptyPressedFrame = preload ("res://media/UI/Pressed - Card.png")


func SetFrames():
	$Frame1.texture_normal = FrameSet[CurrentAttacksAvailable[0]][0]
	$Frame1.texture_pressed = FrameSet[CurrentAttacksAvailable[0]][1]
	$Frame2.texture_normal = FrameSet[CurrentAttacksAvailable[1]][0]
	$Frame2.texture_pressed = FrameSet[CurrentAttacksAvailable[1]][1]
	$Frame3.texture_normal = FrameSet[CurrentAttacksAvailable[2]][0]
	$Frame3.texture_pressed = FrameSet[CurrentAttacksAvailable[2]][1]


func _on_Level_startGame():
	set_process_input(true)
	RandomizeAttacks()
	print("controls on")
	pass # Replace with function body.


func _on_Level_endGame():
	set_process_input(false)
	print("controls off")
	pass
