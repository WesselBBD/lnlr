extends Node

#config

var zcounter: int

var GlobalHealthMultiplier = 1.0
var GlobalSpeedMultiplier = 1.0

const AbilityCooldown = 1.9

const LazerTimeout = 1
const LazerDamage = 10

const BouncerTimeout = 20
const BouncerDamage = 12

const FogPeriod = 2
const FogSlowLingerTime = 0.0
const FogSpeedReduction = 0.6 #0.6 would leave the enemy at 60% of their original speed or a 40% reduction
const FogDamage = 2

const FoggerTimeout = 4

const DiscoDamage = 999

const WooferDamage = 6

const SparksLingerTime = 6

const EnergizedSpeedIncrease = 1.4 #1.4 would leave the enemy with 140% of their energy or give them a 40% boost
const EnergizedEffectLingerTime = 0

#-------

const AttackOffset = 3

enum PlayerAttacks {LAZER, FOG, BOUNCER, DISCO, WOOFER}
enum AOE {SLOW, ENERGIZED}
enum Enemies {ENFORCER, ST0RM, BRUISR}
#var AttackDamages = [LazerDamage, FogDamage, BouncerDamage, DicoDamage, WooferDamage]

var score = 0

const SpawnSafePoint = Vector2.ZERO #should be set somewhere else

var TilesLoaded = false 

enum Lanes {LEFT, CENTER, RIGHT}
enum Areas {FAR, NEAR, CLOSE}
enum Sections {A, B, C}
const SafeLaneId = Lanes.CENTER

const blueTile = "BlueFloor"
const greenTile = "GreenFloor"
const redTile = "RedFloor"

var Lines: Array = [Array(), Array(), Array()]
var Blocks: Array = [Array(), Array(), Array()]

func LineToLaneID(lineName: String) -> int:
	match lineName:
		blueTile: 
			return Lanes.LEFT
		greenTile: 
			return Lanes.CENTER
		redTile:
			return Lanes.RIGHT
	return 0

func GetRandomLane():
	var randId = randi() % Lanes.size()
	return randId

func MakeLaneCopy(lane) -> Array:
	return Lines[lane].duplicate(true)

var SectionSize

func CreateSections():
	var blockSize = Blocks.size()
	SectionSize = (Lines[1].size() / blockSize) #ASSUMES ALL LINES ARE EQUAL
	var prevStart = 0
	for n in range(blockSize):
		var block = Array()
		var newEnd = (n+1)*SectionSize
		for line in Lines:
			for i in range(prevStart,newEnd):
				block.append(line[i])
		Blocks[n] = block
		prevStart = newEnd
	print("Sections Created!")

func GetRandomPositionInBlock(BlockID: int) -> Vector2:
	var randPos = randi() % Blocks[BlockID].size()
	return Blocks[BlockID][randPos]

func GetRandomPositionInLaneBlock(LaneID,BlockID) -> Vector2:
	var randPos = randi() % SectionSize
	var offset = (LaneID*SectionSize)
	return Blocks[BlockID][offset + randPos]

func SegmentToArea(segement):
	#add mapping here if required
	return segement

func SegementToLane(segment):
	#add mapping here if required
	return segment

#I keep forgetting yield(get_tree().create_timer(time), "timeout")
